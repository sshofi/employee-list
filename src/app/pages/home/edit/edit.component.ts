import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmModalComponent } from '@shared/component/confirm-modal/confirm-modal.component';
import { CacheService, EmployeeService } from '@shared/services';
import * as _ from 'lodash';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  @ViewChild(ConfirmModalComponent, { static: true })
  confirmModal: ConfirmModalComponent;
  titleModal;
  messageModal;
  submitLabelModal;
  id;

  fg: FormGroup;

  namePattern = /^[^\s]+(\s+[^\s]+)*$/;

  isProcessing;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private employeeSrv: EmployeeService,
    private fb: FormBuilder,
    public cache: CacheService
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.id = params.id;
      console.log('this.id', this.id);
    });
    this.initForm();
  }

  ngOnInit(): void {
    this.getData(this.fg);
  }

  getData(fg: FormGroup) {
    this.employeeSrv.getDataById(this.id).subscribe(
      (res) => {
        fg.patchValue({ name: res.data.first_name + ' ' + res.data.last_name });
      },
      (err) => {}
    );
  }

  initForm() {
    this.fg = this.fb.group({
      name: [null, [Validators.required, Validators.pattern(this.namePattern)]],
      job: [null, [Validators.required, Validators.pattern(this.namePattern)]],
    });
  }

  checkSubmit() {
    if (this.fg.invalid) {
      const controls = this.fg.controls;
      _.forEach(controls, (control) => {
        control.markAsTouched(true);
        control.markAsDirty(true);
      });
    } else {
      this.confirmModal.showModal();
      this.titleModal = 'Ubah Data Karyawan';
      this.messageModal = 'Apakah anda yakin ingin mengubah data karyawan?';
      this.submitLabelModal = 'Ya';
    }
  }

  editEmployee() {
    // this.form.isProcessing = true;
    const data = this.fg.value;
    console.log(data);
    this.employeeSrv.updateData(this.id, data).subscribe(
      (res) => {
        alert(`Data berhasil diubah menjadi ${res.name}, ${res.job}`);
        this.router.navigate(['home/list']);
      },
      (error) => {
        alert('Terjadi Kesalahan');
      }
    );
  }
}
