import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Page, Sort } from '@shared/models';
import { EmployeeService, ExampleService } from '@shared/services';
import { ConfirmModalComponent } from '@shared/component/confirm-modal/confirm-modal.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
