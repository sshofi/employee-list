import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmModalComponent } from '@shared/component/confirm-modal/confirm-modal.component';
import { CacheService, EmployeeService } from '@shared/services';
import * as _ from 'lodash';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  fg: FormGroup;
  id;

  constructor(
    private fb: FormBuilder,
    public cache: CacheService,
    private router: Router,
    private employeeSrv: EmployeeService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.id = params.id;
    });
    this.initForm();
  }

  ngOnInit(): void {
    this.getData(this.fg);
  }

  getData(fg: FormGroup) {
    this.employeeSrv.getDataById(this.id).subscribe(
      (res) => {
        console.log('RESNYA', res);
        fg.patchValue({ first_name: res.data.first_name });
        fg.patchValue({ last_name: res.data.last_name });
        fg.patchValue({ email: res.data.email });
      },
      (err) => {}
    );
  }
  initForm() {
    this.fg = this.fb.group({
      first_name: [null],
      last_name: [null],
      email: [null],
    });
  }

  checkSubmit() {
    if (this.fg.invalid) {
      const controls = this.fg.controls;
      _.forEach(controls, (control) => {
        control.markAsTouched(true);
        control.markAsDirty(true);
      });
    } else {
      this.submit(this.fg.value);
    }
  }

  submit(data?) {
    this.employeeSrv.updateData(this.id, data).subscribe(
      (res) => {
        alert('Member Berhasil Diubah!');
        this.fg.reset();
      },
      (error) => {
        alert('Terjadi Kesalahan');
      }
    );
  }

  editEmployee() {
    console.log('yha clicked');
    this.router.navigate(['home/edit/' + this.id]);
  }
}
