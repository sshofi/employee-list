import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmModalComponent } from '@shared/component/confirm-modal/confirm-modal.component';
import { CacheService, EmployeeService } from '@shared/services';
import * as _ from 'lodash';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent implements OnInit {
  titleModal;
  messageModal;
  submitLabelModal;

  fg: FormGroup;

  minAge = 7;
  maxAge = 100;
  today = new Date();
  maxDate;
  minDate;

  phoneRegex = /^(^\+62\s?|^08)([0-9])/;
  namePattern = /^[^\s]+(\s+[^\s]+)*$/;
  emailRegex = /^[^\s]+(\s+[^\s]+)*$/;

  isProcessing;

  constructor(
    private fb: FormBuilder,
    public cache: CacheService,
    private router: Router,
    private employeeSrv: EmployeeService
  ) {
    this.initForm();
  }

  ngOnInit(): void {}

  initForm() {
    this.fg = this.fb.group({
      name: [null, [Validators.required, Validators.pattern(this.namePattern)]],
      job: [null, [Validators.required, Validators.pattern(this.namePattern)]],
    });
  }

  checkSubmit() {
    if (this.fg.invalid) {
      const controls = this.fg.controls;
      _.forEach(controls, (control) => {
        control.markAsTouched(true);
        control.markAsDirty(true);
      });
    } else {
      this.submit(this.fg.value);
    }
  }

  submit(body) {
    // this.form.isProcessing = true;
    this.employeeSrv.createData(body).subscribe(
      (res) => {
        alert(`Karyawan ${res.name}, ${res.job} Berhasil Ditambahkan!`);
        this.router.navigate(['home/list']);
      },
      (error) => {
        alert('Terjadi Kesalahan');
      }
    );
  }
  keyPressNum(event: any) {
    const pattern = /(^(^\d|^\W))/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
