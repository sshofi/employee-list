import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmModalComponent } from '@shared/component/confirm-modal/confirm-modal.component';
import { Page, Sort } from '@shared/models';
import { EmployeeService } from '@shared/services';
import * as _ from 'lodash';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @ViewChild(ConfirmModalComponent, { static: true })
  confirmModal: ConfirmModalComponent;
  @ViewChild('memberTable') memberTable: any;

  titleModal;
  messageModal;
  submitLabelModal;
  selectedMemberId;

  loadingResult = false;
  searchForm: FormGroup;

  rows: any[] = [];
  allMemberList;
  tempMemberList;
  offset = 0;
  totalRows;
  previousOrderBy = null;

  page: Page = {
    page: 1,
    row: 5,
  };

  sort: Sort = {
    orderBy: null,
    orderType: null,
  };

  filterParams = {
    first_name: [null],
    email: [null],
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private employeeSrv: EmployeeService,
    public fb: FormBuilder,
    private router: Router
  ) {
    this.searchForm = fb.group({
      first_name: [null],
      email: [null],
    });

    this.page.page = 1 + this.offset;
    this.page.row = 5;

    this.activatedRoute.queryParamMap.subscribe((params: any) => {
      this.offset = params.params.page ? Number(params.params.page) - 1 : 0;
      this.filterParams = {
        first_name: params.params.first_name ? params.params.first_name : null,
        email: params.params.email ? params.params.email : null,
      };
      this.sort = {
        orderBy: params.params.order_by ? params.params.order_by : 'created_at',
        orderType: params.params.order_type ? params.params.order_type : 'desc',
      };
      this.previousOrderBy = params.params.order_by ? params.params.order_by : null;

      Object.keys(this.filterParams).forEach(
        (k) =>
          this.filterParams[k] === null &&
          delete this.filterParams[k] &&
          this.filterParams[k] === undefined &&
          delete this.filterParams[k]
      );
    });

    this.page.page = 1 + this.offset;
    this.page.row = 5;
    this.router.navigate([], { queryParams: { page: this.page.page }, queryParamsHandling: 'merge' });
    this.searchForm.patchValue({
      first_name: this.filterParams.first_name,
      email: this.filterParams.email,
    });
  }

  ngOnInit() {
    this.getData(this.filterParams, this.sort);
  }

  getData(body?, sort?) {
    this.loadingResult = true;
    console.log('this.page', this.page);
    this.employeeSrv.getData(this.page, sort, body).subscribe((res) => {
      console.log('getData employee');
      this.allMemberList = res.data;
      this.rows = this.allMemberList;
      this.tempMemberList = this.rows;

      if (body.first_name) {
        this.tempMemberList = _.filter(this.tempMemberList, (o) => {
          return o.first_name.toLowerCase().includes(body.first_name.toLowerCase());
        });
      }

      if (body.email) {
        this.tempMemberList = _.filter(this.tempMemberList, (o) => {
          return o.email.toLowerCase().includes(body.email.toLowerCase());
        });
      }

      console.log('this.tempMemberList', this.tempMemberList);
      this.totalRows = body.email || body.first_name ? this.rows.length : res.total;
      this.loadingResult = false;
    });
  }

  setPage(value) {
    this.offset = value.offset;
    this.page.page = value.offset + 1;
    this.router.navigate([], { queryParams: { page: this.page.page }, queryParamsHandling: 'merge' });
    this.getData(this.filterParams, this.sort);
  }

  redirectDetail(memberId?) {
    this.router.navigate(['home/detail/' + memberId]);
  }

  redirectAdd() {
    this.router.navigate(['home/add']);
  }

  selectedRow(memberId, firstName, lastName) {
    this.selectedMemberId = memberId;
    this.submitLabelModal = 'Ya';
    this.titleModal = 'Hapus Member';
    this.messageModal = `Apakah anda yakin ingin menghapus member '${firstName + ' ' + lastName}' ?`;
    this.confirmModal.showModal();
  }

  actionSelectedCampaign() {
    this.employeeSrv.deleteData(this.selectedMemberId).subscribe(
      (data) => {
        alert(`Anda telah berhasil menghapus data karyawan dengan id ${this.selectedMemberId}`);
      },
      (error) => {}
    );
  }

  filterName(name) {
    const trimPackageName = name.trim();
    this.searchForm.controls.first_name.patchValue(trimPackageName);
    this.filterParams.first_name = this.searchForm.value.first_name;
    this.router.navigate([], {
      queryParams: {
        page: this.page.page,
        first_name:
          this.searchForm.value.first_name === null || this.searchForm.value.first_name.trim() === ''
            ? null
            : this.searchForm.value.first_name,
      },
      queryParamsHandling: 'merge',
    });
    this.removeNullAndUndefined();
    this.getData(this.filterParams, this.sort);
  }

  filterEmail(email) {
    const trimPackageName = email.trim();
    this.searchForm.controls.email.patchValue(trimPackageName);
    this.filterParams.email = this.searchForm.value.email;
    this.router.navigate([], {
      queryParams: {
        page: this.page.page,
        email:
          this.searchForm.value.email === null || this.searchForm.value.email.trim() === ''
            ? null
            : this.searchForm.value.email,
      },
      queryParamsHandling: 'merge',
    });
    this.removeNullAndUndefined();
    this.getData(this.filterParams, this.sort);
  }

  removeNullAndUndefined() {
    Object.keys(this.filterParams).forEach(
      (k) =>
        this.filterParams[k] === null &&
        delete this.filterParams[k] &&
        this.filterParams[k] === undefined &&
        delete this.filterParams[k]
    );
  }
}
