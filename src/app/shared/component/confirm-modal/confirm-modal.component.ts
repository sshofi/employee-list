import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent implements OnInit {
  @ViewChild('autoShownModal', { static: false })
  autoShownModal: ModalDirective;
  @Output() submitButton = new EventEmitter();
  @Output() cancelButton = new EventEmitter();
  @Input() title = 'Konfirmasi';
  @Input() message: string;
  @Input() submitLabel = 'OK';

  isModalOpen = false;

  constructor() {}

  ngOnInit() {}

  showModal() {
    this.isModalOpen = true;
  }

  hideModal() {
    this.autoShownModal.hide();
  }

  onHidden() {
    this.cancelButton.emit();
    this.isModalOpen = false;
  }

  onSubmit() {
    this.submitButton.emit();
    this.hideModal();
  }
}
