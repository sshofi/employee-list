import { Injectable } from '@angular/core';
import { Page, Sort, Example } from '@shared/models';
import { Observable } from 'rxjs';
import { ApiService } from './core/api.service';
import { GlobalService } from './global.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private api: ApiService) {}

  getData(pagination: Page = null, ordering: Sort = null, filter: object = null): Observable<any> {
    return this.api.getData(`users`, pagination, ordering, filter).pipe(
      tap((data) => {
        return data;
      })
    );
  }

  getDataById(id: string): Observable<any> {
    return this.api.getData(`users/${id}`).pipe(
      tap((data) => {
        return data;
      })
    );
  }

  createData(body: Example): Observable<any> {
    return this.api.postData(`users`, body).pipe(
      tap((data) => {
        return data;
      })
    );
  }

  updateData(id: any, body: Example): Observable<any> {
    return this.api.putData(`users/${id}`, body).pipe(
      tap((data) => {
        return data;
      })
    );
  }

  deleteData(id: any): Observable<any> {
    return this.api.deleteData(`users/${id}`).pipe(
      tap((data) => {
        return data;
      })
    );
  }
}
