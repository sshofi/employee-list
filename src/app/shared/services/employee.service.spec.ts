import { TestBed } from '@angular/core/testing';

import { EmployeeService } from './employee.service';
import { JwtAuthModule } from 'src/app/jwt-auth.module';
import { HttpClientModule } from '@angular/common/http';

describe('EmployeeService', () => {
  let service: EmployeeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JwtAuthModule, HttpClientModule],
    });
    service = TestBed.inject(EmployeeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
