import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmModalComponent } from './component/confirm-modal/confirm-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [ConfirmModalComponent],
  imports: [CommonModule, ModalModule.forRoot()],
  exports: [ConfirmModalComponent],
})
export class SharedModule {}
